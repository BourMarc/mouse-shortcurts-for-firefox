function Point(x, y) {
    this.x = x;
    this.y = y;
}

function Vector(x1, y1, x2, y2) {
    this.x = x2 - x1;
    this.y = y2 - y1;
    /* l is length of trajectory 
    no square root to get the length of the trajectory, because it would be slightly slower */
    this.l = (this.x * this.x) + (this.y * this.y);
    /* Angle in radians */
    if(this.l > 80) {
        if(this.x <= -5) {
            this.a = Math.atan(this.y / this.x) - 3.1416;
        }
        else if (this.x >= 5) {
            this.a = Math.atan(this.y / this.x);
        }
        else if ( this.y > 0 ) { 
            this.a= 1.5709;
        }
        else {
            this.a = - 1.5709;
        }
    }
}

//Look for a link and save its url
Link = (element) => {
    for(let i = 0; !!element && ( i < 20 ); i++) {
        if ((element.tagName === 'A') && element.href) {
            this.href = element.href.toLowerCase();
            if (href.startsWith('javascript')) {
              return false;
            }
            return element;
          } else {
            element = element.parentNode;
          }
        }
    return false;    
}

//List of variables
const start_p = new Point();
const end_p = new Point();
let ordo, context ;
let IsLink;
//End of list of variables

//what happen at beginning of the gesture
function starting_point(event) {
    if (event.button === 2) {
        start_p.x = event.clientX;
        start_p.y = event.clientY;
        IsLink = Link(event.target);
    }
}

//what happend at finishing the gesture
function ending_point(event) {
    ordo = NaN; context = false;
    if (event.button === 2) {
        end_p.x = event.clientX;
        end_p.y = event.clientY;
        let traj = new Vector(start_p.x, start_p.y, end_p.x, end_p.y);
        if (traj.l > 80) {
            context = true;
            ordo = - Math.round(traj.a/(Math.PI/4)) + 2;
            if (ordo === 8) { ordo = 0; }
            }
        switch (ordo) {
            case NaN :
            break
            case 6 : //history back
            window.history.go(-1);
            break
            case 2 : //history forward
            window.history.go(+1);
            break
            case 7 : //scroll to bottom
            window.scrollTo(0, document.body.scrollHeight);
            break
            case 5 : //scroll to top
            window.scrollTo(0, 0);
            break
            default :
            break  
        }
        if (!IsLink) { //Action if not a link
            switch (ordo) {
                case NaN :
                break
                case 3 : //reload
                location.reload(true);
                break
                case 4 : //new tab
                browser.runtime.sendMessage({ordo: 04});
                break
                case 0 : //close tab
                browser.runtime.sendMessage({ordo: 00});
                break
                case 1 : //open last closed tab
                browser.runtime.sendMessage({ordo: 01});
                break
                default :
                break
            }
        }
        else { //Action if link
            switch (ordo) {
                case NaN :
                break
                case 3 : //link open in new background tab
                browser.runtime.sendMessage({ordo: 13, url:IsLink.href});
                break
                case 4 : //link open in new active tab
                browser.runtime.sendMessage({ordo: 14, url:IsLink.href});
                break
                case 0 : //link open in new window
                browser.runtime.sendMessage({ordo: 10, url:IsLink.href});
                break
                case 1 : //link open in new private window
                browser.runtime.sendMessage({ordo: 11, url:IsLink.href});
                break
                default :
                break
            }
        }
    }
}

//Try to prevent contextmenu on Linux or MacOS
function f_context(event) {
    if (context) { event.preventDefault(); event.stopPropagation(); } 
    else { context = true; }
}

document.addEventListener('mousedown', starting_point, true);
document.addEventListener('contextmenu', f_context, true)
document.addEventListener('mouseup', ending_point, true);