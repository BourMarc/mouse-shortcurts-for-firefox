browser.runtime.onMessage.addListener(action);

function action(message) {
    switch (message.ordo) {
        case 04 : //new tab
        browser.tabs.create({});
        break
        case 00 : //close current tab
        browser.tabs.query(
            {
                active: true,               
                lastFocusedWindow: true
            }, 
            function(tabs) {
                browser.tabs.remove(tabs[0].id);
            }
        );
        break
        case 01 : //reopen last closed tab
        browser.windows.getCurrent();
        let recentlyclosed  = browser.sessions.getRecentlyClosed();
        browser.sessions.restore(recentlyclosed[0]);        
        break
        case 14 : //link open in new active tab
        browser.tabs.create({url: message.url, active: true});
        break
        case 13 : //link open in new background tab
        browser.tabs.create({url: message.url, active: false});
        break
        case 10 :
        browser.windows.create({url: message.url});
        break
        case 11 :
        browser.windows.create({url: message.url, incognito: true});
        break
        default :
        break
    }
}