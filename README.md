# Mouse shortcurts for firefox

*Eight Mouse Gesture* is a simple mouse shortcuts add-on, or mouse gestures extension, for Firefox. Just by straight mouse gestures, you can open recently closed tab, open a link in a new tab (new window or private window), go back and forth in history, scroll to the top or bottom of a page, close a tab and reload a page.

## Add mouse shortcuts to Firefox

In order to use *Eight Mouse Gestures*, and get more details, please visit [Mouse Shortcuts for Firefox](https://addons.mozilla.org/en-US/firefox/addon/eight-mouse-gestures/), in official addons.mozilla.org website.




